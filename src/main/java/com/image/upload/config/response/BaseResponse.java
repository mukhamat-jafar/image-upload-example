package com.image.upload.config.response;

import com.image.upload.model.ImageEntity;
import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@Data
public class BaseResponse<T> {
    private boolean status;
    private T data;
    private String message;

    public BaseResponse(boolean status, T data, String message) {
        this.status = status;
        this.data = data;
        this.message = message;
    }

}
