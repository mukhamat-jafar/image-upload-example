package com.image.upload.model;

import com.image.upload.auditable.Auditable;
import com.sun.istack.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@Data
@NoArgsConstructor
@Entity
@Table(name = "tbl_image")
public class ImageEntity extends Auditable<String> {

    @Column(name = "name", length = 20)
    private String name;

    @Column(name = "description", length = 20)
    private String description;

    @NotEmpty(message = "Image may not be empty")
    @Column(name = "image")
    private String image;
}
