package com.image.upload.dto.Image;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ImageDto {

    private Long id;
    private String name;
    private String description;
    private String image;

    public ImageDto(String name, String description, String image) {
        this.name = name;
        this.description = description;
        this.image = image;
    }

//    public ImageDto(Long id, Object o1, Object o2) {
//    }
}
