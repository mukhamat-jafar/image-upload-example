package com.image.upload.service;

import com.image.upload.config.error.FileNotFoundException;
import com.image.upload.config.response.BaseResponse;
import com.image.upload.dto.Image.CreateImageDto;
import com.image.upload.dto.Image.ImageDto;
import com.image.upload.model.ImageEntity;
import com.image.upload.repository.ImageRepository;
import org.apache.logging.log4j.message.Message;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MultipartFile;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class ImageService {
    @Autowired
    private ImageRepository imageRepository;

    @Autowired
    private ModelMapper modelMapper;

    //Get All Data Image
    public ResponseEntity<List<ImageEntity>> listImageEntity() {
        List<ImageEntity> imageEntities= imageRepository.findAll();
        Type targetType = new TypeToken<List<ImageEntity>>() {}.getType();
        List<ImageEntity> response = modelMapper.map(imageEntities, targetType);

        return ResponseEntity.ok(response);
    }

    //Get Image By ID
    public ResponseEntity<ImageEntity> getImageById(Long id) {
        ImageEntity imageEntity = imageRepository.findById(id).orElseThrow(()-> new FileNotFoundException("Image id " + id + " is not exist"));
        ImageEntity response = modelMapper.map(imageEntity, ImageEntity.class);

        return ResponseEntity.ok(response);
    }

    //POST Data Image
    public ResponseEntity<ImageEntity> addImage(MultipartFile file, ImageEntity newImage) {
        String message = "Image only PNG & JPG";

        String jpgMimeType = "image/jpeg", pngMimeType = "image/png";
        if (!file.getContentType().equals(jpgMimeType)
            && !file.getContentType().equals(pngMimeType)){
            return new ResponseEntity<>(HttpStatus.valueOf(message));
        }

        ImageEntity imageEntity = new ImageEntity();

        imageEntity.setName(file.getContentType());
        imageEntity.setDescription(newImage.getDescription());
        imageEntity.setImage(file.getOriginalFilename());

        ImageEntity response = imageRepository.save(imageEntity);

        return ResponseEntity.ok(response);
    }

    //Put Data By Id
    public ResponseEntity<ImageEntity> editImage(ImageEntity updateImage, Long id) {
        ImageEntity entity = imageRepository.findById(id).orElseThrow(()-> new FileNotFoundException("Image id " + id + " is not exist"));

        entity.setName(updateImage.getName());
        entity.setDescription(updateImage.getDescription());
        entity.setImage(updateImage.getImage());

        imageRepository.save(entity);
        ImageEntity response = modelMapper.map(entity, ImageEntity.class);

        return ResponseEntity.ok(response);
    }

    //Delete Data By Id
    public ResponseEntity<ImageEntity> deleteImage(Long id) {
        ImageEntity entity = imageRepository.findById(id).orElseThrow(()-> new FileNotFoundException("Image id " + id + " is not exist"));
        imageRepository.deleteById(id);
        ImageEntity response = modelMapper.map(entity, ImageEntity.class);

        return ResponseEntity.ok(response);
    }
}
